FROM rust:1-alpine3.13 AS build

RUN cargo install envsub --version 0.1.3

FROM php:7.4.24-fpm-alpine

ARG WALLABAG_VERSION=2.4.2
ARG BUILD_DATE
ARG VCS_REF
ARG VCS_BRANCH

WORKDIR /var/www/html

ENV LD_PRELOAD /usr/lib/preloadable_libiconv.so php
ENV SYMFONY_ENV=prod

RUN set -ex; \
    \
    apk add --no-cache --virtual .run-deps \
        gnu-libiconv=1.16-r0 \
        imagemagick6-libs \
        tzdata \
    ; \
    apk add --no-cache --virtual .build-deps \
        $PHPIZE_DEPS \
        freetype-dev \
        gettext-dev \
        icu-dev \
        imagemagick6-dev \
        libjpeg-turbo-dev \
        libpng-dev \
        libxml2-dev \
        libzip-dev \
        oniguruma-dev \
        postgresql-dev \
        sqlite-dev \
        tidyhtml-dev \
    ; \
    docker-php-ext-configure gd --with-freetype --with-jpeg; \
    docker-php-ext-install -j "$(nproc)" \
       bcmath \
       gd \
       gettext \
       iconv \
       intl \
       mbstring \
       opcache \
       pdo \
       pdo_mysql \
       pdo_pgsql \
       pdo_sqlite \
       sockets \
       tidy \
       zip \
    ; \
    pecl install redis; \
    pecl install imagick; \
    docker-php-ext-enable \
       redis \
       imagick \
    ; \
    runDeps="$( \
        scanelf --needed --nobanner --format '%n#p' --recursive /usr/local/lib/php/extensions \
            | tr ',' '\n' \
            | sort -u \
            | awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }' \
    )"; \
    apk add --virtual .wallabag-phpext-rundeps $runDeps; \
    apk del .build-deps \
    ; \
    apk add --virtual .composer-runtime-deps git patch; \
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin/ --filename=composer; \
    rm -rf /usr/src/* /tmp/pear/*

# TODO checksum
RUN wget -O /tmp/wallabag.tar.gz https://github.com/wallabag/wallabag/archive/$WALLABAG_VERSION.tar.gz; \
    mkdir /tmp/extract; \
    tar xf /tmp/wallabag.tar.gz -C /tmp/extract; \
    rmdir /var/www/html; \
    mv /tmp/extract/wallabag-*/ /var/www/html; \
    cd /var/www/html; \
    composer install --no-dev --no-interaction -o --prefer-dist; \
    chown -R 1001:1001 /var/www/html; \
    rm -rf /tmp/wallabag.tar.gz /tmp/extract /root/.composer /var/www/html/var/cache/prod;

RUN set -ex \
    && addgroup -g 1001 -S nginx \
    && adduser -S -D -H -u 1001 -h /var/cache/nginx -s /sbin/nologin -G nginx -g nginx nginx \
    && apk add --no-cache nginx multirun \
    # So non-root can execute multirun
    && chmod +x /usr/bin/multirun

COPY docker-entrypoint.sh /usr/local/bin/
COPY config/ /opt/wallabag/config
COPY --from=build /usr/local/cargo/bin/envsub /usr/local/bin/

RUN chown -R 1001:0 /etc/nginx \
        /usr/local/etc/php/conf.d \
        /opt/wallabag/config \
        /var/www/html/app/config \
    && chmod -R g+w /etc/nginx

VOLUME [ "/opt/wallabag/config" ]

LABEL \ 
	build-date=$BUILD_DATE \
	vcs-branch=$VCS_BRANCH \
	vcs-commit=$VCS_REF

EXPOSE 8080

USER 1001

ENTRYPOINT ["docker-entrypoint.sh"]
CMD ["multirun", "nginx", "php-fpm"]
