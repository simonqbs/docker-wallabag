# Wallabag Docker Image

A Wallabag docker image complete with nginx and wallabag. This docker image is based on a [PR](https://github.com/wallabag/docker/pull/256), and the official images configurations with bits and pieces copied from all over the 'net.

## Usage

```
docker run -it --rm -e SYMFONY__ENV__DOMAIN_NAME=http://localhost:8080 -p 8080:8080 registry.gitlab.com/simonqbs/wallabag
```

## Configuration

Environment variables are used for configuration. See the files in [config/](./config) folder for possible values. It might also be possible to bring your own configuration files since the configuration folder is exposed as a volume.

### Example changing secret key

```
docker run \
  -it \
  --rm \
  -p 8080:8080 \
  -e SYMFONY__ENV__SECRET=mysecret \
  registry.gitlab.com/simonqbs/wallabag
```

## Versioning

```
0.1.0-wb-2.4.2

where 0.1.0 is the version of this image
where wb-2.4.2 is which wallabag version is used

```

## TODO

- More configuration examples
- How to use with a database (postgresql)
- Volumes (cache?), possible to use read-only root?
- Write a few words about why not just use the official image (something about database stuff)