#!/bin/sh
envsub -g < /opt/wallabag/config/wallabag-php.ini > /usr/local/etc/php/conf.d/wallabag-php.ini
envsub -g < /opt/wallabag/config/parameters.yml > /var/www/html/app/config/parameters.yml
envsub -v 'NGINX_CLIENT_MAX_BODY_SIZE' -v 'NGINX_WORKER_PROCESSES' < /opt/wallabag/config/nginx.conf > /etc/nginx/nginx.conf
cp /opt/wallabag/config/fastcgi_params /etc/nginx/

/var/www/html/bin/console wallabag:install --env=prod --no-interaction

if [ "$1" = "migrate" ]; then
    exec /var/www/html/bin/console doctrine:migrations:migrate --env=prod --no-interaction
fi

exec "$@"
